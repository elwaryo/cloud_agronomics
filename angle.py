def absolute_angle(hour, minute):
    hour_increment = minute/60.0
    hour_angle = 360.0 * (hour % 12.0 + hour_increment) / 12.0
    minute_angle = 360.0 * minute / 60
    angle_difference = abs(hour_angle - minute_angle)
    return angle_difference
