from unittest import TestCase

from angle import absolute_angle


class Test_absolute_angle(TestCase):

    # See https://www.visnos.com/demos/clock

    def test_1(self):
        self.assertEqual(90.0, absolute_angle(3, 0))

    def test_2(self):
        self.assertEqual(180.0, absolute_angle(6, 0))

    def test_3(self):
        self.assertEqual(270.0, absolute_angle(9, 0))

    def test_4(self):
        self.assertEqual(245.0, absolute_angle(1, 50))

    def test_5(self):
        self.assertEqual(0.0, absolute_angle(12, 0))

    def test_6(self):
        self.assertEqual(5.5, absolute_angle(12, 1))

    def test_7(self):
        self.assertEqual(55.0, absolute_angle(12, 10))

    def test_8(self):
        self.assertEqual(110.0, absolute_angle(12, 20))

    def test_9(self):
        self.assertEqual(165.0, absolute_angle(12, 30))

    def test_10(self):
        self.assertEqual(220, absolute_angle(12, 40))

    def test_11(self):
        self.assertEqual(275.0, absolute_angle(12, 50))

    def test_12(self):
        self.assertEqual(245.0, absolute_angle(1, 50))

    def test_13(self):
        self.assertEqual(160.0, absolute_angle(2, 40))

    def test_14(self):
        self.assertEqual(72.5, absolute_angle(7, 25))

    def test_15(self):
        self.assertEqual(190.0, absolute_angle(10, 20))

    def test_16(self):
        self.assertEqual(245.0, absolute_angle(1, 50))

    def test_17(self):
        self.assertEqual(12.5, absolute_angle(5, 25))

    def test_18(self):
        self.assertEqual(157.5, absolute_angle(8, 15))

    def test_19(self):
        self.assertEqual(150.0, absolute_angle(5, 0))

    def test_20(self):
        self.assertEqual(120.0, absolute_angle(4, 0))
